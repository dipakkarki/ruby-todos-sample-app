# Ruby on Rails Todo Application deployed with Ansible in AWS
Note that this Repo is still in development mode, please DO NOT USE IN PRODUCTION SYSTEM. Thanks to Mingdong Luo for his ruby application (https://github.com/mdluo/todos-rails)

# About the ansible playbooks
- All the ansible playbooks are located inside the ansible/playbooks folder. 
- To set the value for the variables used in the ansible playbook, copy ./ansible/group_vars/all.sample to ./ansible/group_vars/<host>
- Configure ./ansible/host file with the EC2 host information.
- install the following ansible dependencies:
  - ansible-galaxy install geerlingguy.passenger
  - ansible-galaxy collection install community.aws 
- Ansible playbooks 
  - elb.yml (this playbook configures amazon AWS load balancer)
  - app.yml (this playbook installs ruby and all the dependencies required to run the ruby application in EC2 instance)
  - rds.yml (this playbook configures amazon RDS with postgresql engine)
  - webserver.yml (this playbook configures passanger , a ruby web server and nginx)
- How to execute playbooks. 
  - ansible-playbook ./ansible/playbooks/<playbook>.yml 

# Development Roadmap Version 2.0
- Upgrade the todos application to Ruby 2.7.0 
- Use ALB (Application Load Balancer) instead of (ELB) Elastic load balancer and configure vpc and target groups , as Application Load balancer is more flexible and supports more protocol 
- Write backup and restore database with amazon RDS.
- Implement CI / CD pipeline using Amazon Codepipeline, Codecommit, CodeBuild and Lambda function. 
- Implement AWS cloudfront to reduce the latency of the same object fetched multiple times.  #
- Automate and management of EC2 instance using ansible module. 
- Configure AWS secret manager for mangement of the security credential.

# Development Roadmap Version 3.0
- Dockerize the application
- Deploy it to Amazon Kubernetes Service (EKS)
- Reconfigure ALB, CodeBuild to build Docker containers
- Define ALB target group based on the namespace (dev,staging, production) 
- Automatically deploy the latest code  using Kubernetes rolling update defining lambda function. 
- Implement Elasticsearch and fluentd for log and performance matrices.

